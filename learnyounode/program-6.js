var mymodule = require('./mymodule.js');

function callback(error, result) {
	if (error) return console.error(error);

	for (var i = 0; i < result.length; i++) {
		console.log(result[i]);
	}
};

mymodule(process.argv[2], process.argv[3], callback);
