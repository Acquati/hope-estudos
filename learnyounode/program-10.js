var net = require('net');

var server = net.createServer(function(socket) {
  var date = new Date();
  var data = date.getFullYear() + '-' + 
	  ((date.getMonth() + 1) + "").padStart(2,"0") + '-' + 
	  (date.getDate() + "").padStart(2,"0") + ' ' + 
	  (date.getHours() + "").padStart(2,"0") + ':' + 
	  (date.getMinutes() + "").padStart(2,"0") + '\n';

  socket.end(data);
});

server.listen(process.argv[2]);
