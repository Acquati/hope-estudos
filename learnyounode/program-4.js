var fs = require('fs');
var newlines = 0;

fs.readFile(process.argv[2], finishedReading);

function finishedReading(error, fileData) {
	if (error) return console.error(error);

	for (var i = 0; i < fileData.length; i++) {
		if (fileData[i] === 10) {
			newlines++;
		}
	}

	console.log(newlines);
}
