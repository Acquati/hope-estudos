var fs = require('fs');
var path = require('path');

module.exports = function (dirName, extention, callback) {
	fs.readdir(dirName, finishedReading);

	function finishedReading(error, list) {
		if (error) return callback(error, result);

		var result = [];

		for (var i = 0; i < list.length; i++) {
			if (path.extname(list[i]) === '.' + extention) {
				result.push(list[i]);
			}
		}

		callback(null, result);
	};
};
