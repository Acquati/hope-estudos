var fs = require('fs');
var path = require('path');

fs.readdir(process.argv[2], finishedReading);

function finishedReading(error, list) {
	if (error) return console.error(error);

	var result = [];

	for (var i = 0; i < list.length; i++) {
		if (path.extname(list[i]) === '.' + process.argv[3]) {
			result.push(list[i]);
		}
	}

	for (var i = 0; i < result.length; i++) {
		console.log(result[i]);
	}
}
